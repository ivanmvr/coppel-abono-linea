import React from 'react';
import Button from '@material-ui/core/Button';

export default function Payment3(props) {
  return (
    <div className="PaymentContainer">
      <img src={props.image} alt="feature" width="100px" height="100px" className="imgCenter"/>
      <h3>{props.headerName}</h3>
      <ol>
        <li> Un supervisor de crédito Coppel visitará tu domicilio</li>
        <li> Abona en efectivo, no tiene comisión</li>
        <li> Recibe tu folio, éste será tu comprobante</li>
      </ol>
    </div>
  );
}