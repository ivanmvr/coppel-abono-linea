import React from 'react';
import Button from '@material-ui/core/Button';

export default function Payment4(props) {
  return (
    <div className="PaymentContainer">
      <img src={props.image} alt="feature" width="100px" height="100px" className="imgCenter"/>
      <h3>{props.headerName}</h3>
      <ol>
        <li> <a href="">Ubica aquí</a> tu sucursal Telecomm más cercana</li>
        <li> Abona en efectivo a tu cuenta, no tiene comisión</li>
        <li> Recibe tu folio, éste será tu comprobante</li>
      </ol>
    </div>
  );
}