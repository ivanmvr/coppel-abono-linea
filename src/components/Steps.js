import React from 'react';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';

export default function Steps() {

  return (
    <div>
      <h2 className="ContainersHeaders">Sigue estos pasos para abonar en:</h2>
      <div id="StepsContainer">
        <Step1 headerName="Coppel.com"/>
        <Step2 headerName="App Coppel"/>
        <Step3 headerName="WhatsApp"/>
      </div>
    </div> 
  );
}