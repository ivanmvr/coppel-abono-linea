import React from 'react';

export default function Step2(props) {
  return (
    <div className="StepContainer">
      <h3>{props.headerName}</h3>
      <ol>
        <li>Abre la App Coppel en tu celular e inicia sesión, si todavía no tienes cuenta, crea una desde tu aplicación o en Coppel.com</li>
        <li>Entra a la sección Abonos o en Estado de cuenta si ya eres cliente</li>
        <li>Escoge las cuentas que quieres pagar y la cantidad de tu abono.<br/>
            <span className="listGrayText">Para pagar la cuenta de un amigo, amiga o familiar, ingresa el número de cliente al que quieres abonar; lo encuentras al frente de la tarjeta o en estado de cuenta</span></li>
        <li>Paga con cualquier tarjeta bancaria, menos con crédito BanCoppel</li>
      </ol>
      
    </div>
  );
}