import React from 'react';

export default function Payment2(props) {
  return (
    <div className="PaymentContainer">
      <img src={props.image} alt="feature" width="100px" height="100px" className="imgCenter"/>
      <h3>{props.headerName}</h3>
      <ol>
        <li> <a href="">Ubica aquí</a> tu tienda Oxxo más cercana</li>
        <li> En caja, di que quieres pagar tu crédito Coppel, te pedirán tu número de cliente</li>
        <li> Paga con efectivo la cantidad que quieres abonar</li>
        <li> Recuerda que tu dinero primero se irá a las cuentas vencidas y luego se distribuirá en las demás</li>
        <li> La comisión es de $12</li>
        <li> Puedes hacer los abonos que quieras entre $100 y hasta $10,000</li>
      </ol>
      
    </div>
  );
}