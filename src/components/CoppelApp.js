import React from 'react';
import Steps from './Steps';
import Payments from './Payments';
import Benefits from './Benefits';

export default function FreeAgentApp() {
  return (
    <React.Fragment>
      <Steps/>
      <Payments/>
      <Benefits/>
    </React.Fragment>
  );
}
