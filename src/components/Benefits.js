import React from 'react';
import Benefit from './Benefit';

export default function Benefits() {
  return (
    <div>
      <h2 className="ContainersHeaders">Pagar a tiempo tiene grandes beneficios para ti</h2>
      <div id="BenefitsContainer">
        <Benefit url="https://www.blugraphic.com/wp-content/uploads/2014/03/key-icon.jpg" text="Compra a crédito con un mayor plazo y sin pago inicial"/>
        <Benefit url="https://www.blugraphic.com/wp-content/uploads/2014/03/key-icon.jpg" text="Usa tu crédito para comprar en negocios afiliados con Coppel Pay"/>
        <Benefit url="https://www.blugraphic.com/wp-content/uploads/2014/03/key-icon.jpg" text="Solicita préstamos de dinero en cualquier momento en Coppel.com, tu App y en tienda"/>
      </div>
    </div>
  );
}