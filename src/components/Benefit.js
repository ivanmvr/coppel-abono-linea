import React from 'react';

export default function Benefit(props) {
  return (
    <div className="Benefit">
      <img src={props.url} alt="feature" width="100px" height="100px"/> 
      <p>
        {props.text}
      </p>
    </div>
  );
}