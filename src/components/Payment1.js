import React from 'react';
import Button from '@material-ui/core/Button';

export default function Payment1(props) {
  return (
    <div className="PaymentContainer">
      <img src={props.image} alt="feature" width="100px" height="100px" className="imgCenter"/> 
      <h3>{props.headerName}</h3>
      <ol>
        <li> Ve a tu tienda Coppel más cercana</li>
        <li> Acércate a Cajas de Abono o en cualquier caja de ropa o muebles</li>
        <li> Paga con efectivo o tarjeta bancaria</li>
      </ol>
      <div id="Payment1Bttn">
        <Button variant="outlined" color="primary" >Ubica tu tienda</Button><br/>
      </div>
    </div>
  );
}