import React from 'react';
import Button from '@material-ui/core/Button';

export default function Step1(props) {
  return (
    <div className="StepContainer">
      <h3>{props.headerName}</h3>
      <ol>
        <li>Inicia sesión en tu cuenta Coppel, si todavía no la tienes, <a href="">crea una cuenta</a> en este momento</li>
        <li>Si tienes crédito y aún no está ligado a tu cuenta de Coppel.com, usa tu número de cliente para <a href="">ligar tu crédito aquí</a></li>
        <li>Ve a la sección de <a href="">Estado de cuenta</a> y da clic en <b>Abonar a crédito</b></li>
        <li>Escoge las cuentas que quieres pagar y la cantidad de tu abono.<br/>
            <span className="listGrayText">Para pagar la cuenta de un amigo, amiga o familiar, ingresa el número de cliente al que quieres abonar; lo encuentras al frente de la tarjeta o el estado de cuenta</span></li>
        <li>Paga con cualquier tarjeta bancaria, menos con crédito BanCoppel</li>
      </ol>
      <div id="Step1Buttons">
        <Button variant="contained" color="primary" >Inicia sesión para abonar</Button><br/>
        <a href="">Crea una cuenta</a>
      </div>
    </div>
  );
}