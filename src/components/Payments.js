import React from 'react';
import Payment1 from './Payment1';
import Payment2 from './Payment2';
import Payment3 from './Payment3';
import Payment4 from './Payment4';

export default function Payments() {

  return (
    <div>
      <h2 className="ContainersHeaders">Otras formas de abonar:</h2>
      <div id="PaymentsContainer">
        <Payment1 headerName="Tiendas Coppel" image="https://www.revistaporte.com/wp-content/uploads/2018/02/Coppel.png"/>
        <Payment2 headerName="Pago en tienda Oxxo" image="https://www.revistaporte.com/wp-content/uploads/2018/02/Coppel.png"/>
        <Payment3 headerName="Supervisor de crédito" image="https://www.revistaporte.com/wp-content/uploads/2018/02/Coppel.png"/>
        <Payment4 headerName="Telecomm" image="https://www.revistaporte.com/wp-content/uploads/2018/02/Coppel.png"/>
      </div>
    </div> 
  );
}