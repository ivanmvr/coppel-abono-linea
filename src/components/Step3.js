import React from 'react';
import Button from '@material-ui/core/Button';

export default function Step3(props) {
  return (
    <div className="StepContainer">
      <h3>{props.headerName}</h3>
      <ol>
        <li>Registra tu número celular en el <a href="">servicio de WhatsApp</a>, necesitarás tu número de cliente</li>
        <li>Asegúrate de guardar el número y que la cuenta tenga la siguiente insignia verde  de insignia verde de WhatsApp al lado del nombre del contacto, lo cual confirma que es una cuenta verificada por WhatsApp</li>
        <li>Abre una conversación y dinos "Hola"</li>
        <li>Sigue las instrucciones y abona a tus cuentas con una tarjeta de débito BanCoppel</li>
      </ol>
      <div id="Step3Bttn">
        <Button variant="outlined" color="primary" >Registrar mi número</Button><br/>
      </div>
    </div>
  );
}