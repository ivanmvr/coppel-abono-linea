import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import './styles/styles.css';

ReactDOM.render(<AppRouter />, document.querySelector('#app'));